package lightingpoint

import "context"

type Service struct {
	repository Storage
}

func NewService(r Storage) Service {
	return Service{
		repository: r,
	}
}

func (s Service) Create(ctx context.Context, input LightingPoint) (LightingPoint, error) {
	item, err := s.repository.Create(ctx, input)
	if err != nil {
		return LightingPoint{}, err
	}

	return item, nil
}

func (s Service) List(ctx context.Context) ([]LightingPoint, error) {
	list, err := s.repository.List(ctx)
	if err != nil {
		return []LightingPoint{}, err
	}

	return list, nil
}

func (s Service) Get(ctx context.Context, id int) (LightingPoint, error) {
	item, err := s.repository.Get(ctx, id)
	if err != nil {
		return LightingPoint{}, err
	}

	return item, nil
}

func (s Service) Update(ctx context.Context, id int, input LightingPoint) error {
	err := s.repository.Update(ctx, id, input)
	if err != nil {
		return err
	}

	return nil
}

func (s Service) Delete(ctx context.Context, id int) error {
	err := s.repository.Delete(ctx, id)
	if err != nil {
		return err
	}

	return nil
}
