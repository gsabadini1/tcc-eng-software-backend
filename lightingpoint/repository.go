package lightingpoint

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log/slog"
	"strings"
	"time"
)

type Storage interface {
	Create(context.Context, LightingPoint) (LightingPoint, error)
	Update(context.Context, int, LightingPoint) error
	List(context.Context) ([]LightingPoint, error)
	Get(context.Context, int) (LightingPoint, error)
	Delete(context.Context, int) error
}

type Repository struct {
	db *sql.DB
}

func NewRepository(db *sql.DB) Repository {
	return Repository{
		db: db,
	}
}

func (r Repository) Create(ctx context.Context, input LightingPoint) (LightingPoint, error) {
	exec, err := r.db.ExecContext(ctx,
		"INSERT INTO LightingPoint (height, lamp_type, lamp_power, latitude, longitude, created_at) VALUES (?, ?, ?, ?, ?, ?);",
		input.Height,
		input.LampType,
		input.LampPower,
		input.Latitude,
		input.Longitude,
		time.Now(),
	)
	if err != nil {
		slog.Error(err.Error())
		return LightingPoint{}, err
	}

	id, err := exec.LastInsertId()
	if err != nil {
		slog.Error(err.Error())
		return LightingPoint{}, err
	}

	input.ID = int(id)

	return input, nil
}

func (r Repository) Update(ctx context.Context, id int, input LightingPoint) error {
	var args []string

	if input.Height != 0 {
		args = append(args, fmt.Sprintf("height = %f", input.Height))
	}

	if input.LampPower != 0 {
		args = append(args, fmt.Sprintf("lamp_power = '%d'", input.LampPower))
	}

	if input.LampType != "" {
		args = append(args, fmt.Sprintf("lamp_type = '%s'", input.LampType))
	}

	if input.Latitude != "" {
		args = append(args, fmt.Sprintf("latitude = '%s'", input.Latitude))
	}

	if input.Longitude != "" {
		args = append(args, fmt.Sprintf("longitude = '%s'", input.Longitude))
	}

	var query = fmt.Sprintf("UPDATE LightingPoint SET %s WHERE id = ?;", strings.Join(args, ", "))

	_, err := r.db.ExecContext(ctx, query, id)
	if err != nil {
		slog.Error(err.Error())
		return err
	}

	return nil
}

func (r Repository) List(ctx context.Context) ([]LightingPoint, error) {
	var lightingPoints = make([]LightingPoint, 0)

	rows, err := r.db.QueryContext(ctx, "SELECT * FROM LightingPoint WHERE deleted_at IS NULL;")
	if err != nil {
		return []LightingPoint{}, err
	}
	defer rows.Close()

	for rows.Next() {
		var lightingPoint LightingPoint
		var deletedAt sql.NullString

		if err = rows.Scan(
			&lightingPoint.ID,
			&lightingPoint.Height,
			&lightingPoint.LampType,
			&lightingPoint.LampPower,
			&lightingPoint.Latitude,
			&lightingPoint.Longitude,
			&lightingPoint.CreatedAt,
			&deletedAt,
		); err != nil {
			slog.Error(err.Error())
			return []LightingPoint{}, err
		}

		lightingPoints = append(lightingPoints, lightingPoint)
	}

	if err = rows.Err(); err != nil {
		slog.Error(err.Error())
		return []LightingPoint{}, err
	}

	return lightingPoints, nil
}

func (r Repository) Get(ctx context.Context, id int) (LightingPoint, error) {
	var lightingPoint LightingPoint
	var deletedAt sql.NullString

	row := r.db.QueryRowContext(ctx, "SELECT * FROM LightingPoint WHERE id = ? AND deleted_at IS NULL;", id)

	if err := row.Scan(
		&lightingPoint.ID,
		&lightingPoint.Height,
		&lightingPoint.LampType,
		&lightingPoint.LampPower,
		&lightingPoint.Latitude,
		&lightingPoint.Longitude,
		&lightingPoint.CreatedAt,
		&deletedAt,
	); err != nil {
		slog.Error(err.Error())

		if errors.Is(err, sql.ErrNoRows) {
			return LightingPoint{}, err
		}

		return LightingPoint{}, err
	}

	return lightingPoint, nil
}

func (r Repository) Delete(ctx context.Context, id int) error {
	_, err := r.db.ExecContext(ctx, "UPDATE LightingPoint SET deleted_at = ? WHERE id = ?", time.Now(), id)
	if err != nil {
		slog.Error(err.Error())
		return err
	}

	return nil
}
