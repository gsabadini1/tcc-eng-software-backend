package lightingpoint

import (
	"time"
)

type LightingPoint struct {
	ID        int       `json:"id"`
	Height    float64   `json:"height"`
	LampType  string    `json:"lamp_type"`
	LampPower int       `json:"lamp_power"`
	Latitude  string    `json:"latitude"`
	Longitude string    `json:"longitude"`
	CreatedAt time.Time `json:"-"`
	DeletedAt time.Time `json:"-"`
}
