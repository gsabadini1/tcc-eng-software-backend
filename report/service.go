package report

import (
	"context"
	"tcc-software-engineer/serviceorder"
)

type Service struct {
	repository Storage
}

func NewService(r Storage) Service {
	return Service{
		repository: r,
	}
}

func (s Service) GetByStatus(ctx context.Context) (ByStatus, error) {
	reports, err := s.repository.GetByStatus(ctx)
	if err != nil {
		return ByStatus{}, err
	}

	var output = ByStatus{}

	for _, report := range reports {
		if report.Status == serviceorder.Open {
			output.CountOpened = report.Count
		}

		if report.Status == serviceorder.Close {
			output.CountClosed = report.Count
		}
	}

	return output, nil
}
