package report

import (
	"context"
	"database/sql"
	"log/slog"
)

type Storage interface {
	GetByStatus(context.Context) ([]ByStatus, error)
}

type Repository struct {
	db *sql.DB
}

func NewRepository(db *sql.DB) Repository {
	return Repository{
		db: db,
	}
}

func (r Repository) GetByStatus(ctx context.Context) ([]ByStatus, error) {
	var reports = make([]ByStatus, 0)

	rows, err := r.db.QueryContext(ctx, "SELECT status, COUNT(status) FROM ServiceOrder GROUP BY status;")
	if err != nil {
		return []ByStatus{}, err
	}
	defer rows.Close()

	for rows.Next() {
		var report ByStatus

		if err = rows.Scan(
			&report.Status,
			&report.Count,
		); err != nil {
			slog.Error(err.Error())
			return []ByStatus{}, err
		}

		reports = append(reports, report)
	}

	if err = rows.Err(); err != nil {
		slog.Error(err.Error())
		return []ByStatus{}, err
	}

	return reports, nil
}
