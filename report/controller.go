package report

import (
	"net/http"
	"tcc-software-engineer/response"
)

type Controller struct {
	service Service
}

func NewController(s Service) Controller {
	return Controller{
		service: s,
	}
}

func (c Controller) GetByStatus(w http.ResponseWriter, r *http.Request) {
	output, err := c.service.GetByStatus(r.Context())
	if err != nil {
		response.NewResponse(
			response.NewError("error getting report by status"),
			http.StatusInternalServerError,
		).Send(w)
		return
	}

	response.NewResponse(output, http.StatusOK).Send(w)
	return
}
