package report

import "tcc-software-engineer/serviceorder"

type ByStatus struct {
	Status serviceorder.Status `json:"-"`
	Count  int                 `json:"-"`

	CountOpened int `json:"count_opened"`
	CountClosed int `json:"count_closed"`
}
