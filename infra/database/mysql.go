package database

import (
	"database/sql"
	"github.com/go-sql-driver/mysql"
	"log"
)

func Open() *sql.DB {
	cfg := mysql.Config{
		User:      "dev",
		Passwd:    "dev",
		DBName:    "dev",
		Addr:      "localhost:3306",
		Logger:    log.Default(),
		ParseTime: true,
	}

	var err error
	db, err := sql.Open("mysql", cfg.FormatDSN())
	if err != nil {
		log.Fatal(err)
	}

	pingErr := db.Ping()
	if pingErr != nil {
		log.Fatal(pingErr)
	}

	return db
}
