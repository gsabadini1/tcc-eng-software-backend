package user

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log/slog"
	"strings"
	"time"
)

type Storage interface {
	Create(context.Context, User) (User, error)
	Update(context.Context, int, User) error
	List(context.Context) ([]User, error)
	Get(context.Context, int) (User, error)
	GetByEmail(context.Context, string) (User, error)
	Delete(context.Context, int) error
}

type Repository struct {
	db *sql.DB
}

func NewRepository(db *sql.DB) Repository {
	return Repository{
		db: db,
	}
}

func (r Repository) Create(ctx context.Context, input User) (User, error) {
	exec, err := r.db.ExecContext(ctx, "INSERT INTO User (name, email, password, created_at) VALUES (?, ?, ?, ?);",
		input.Name,
		input.Email,
		input.Password,
		time.Now(),
	)
	if err != nil {
		slog.Error(err.Error())
		return User{}, err
	}

	id, err := exec.LastInsertId()
	if err != nil {
		slog.Error(err.Error())
		return User{}, err
	}

	input.ID = int(id)

	return input, nil
}

func (r Repository) Update(ctx context.Context, id int, input User) error {
	var args []string

	if input.Name != "" {
		args = append(args, fmt.Sprintf("name = '%s'", input.Name))
	}

	if input.Password != "" {
		args = append(args, fmt.Sprintf("password = '%s'", input.Password))
	}

	var query = fmt.Sprintf("UPDATE User SET %s WHERE id = ?;", strings.Join(args, ", "))

	_, err := r.db.ExecContext(ctx, query, id)
	if err != nil {
		slog.Error(err.Error())
		return err
	}

	return nil
}

func (r Repository) List(ctx context.Context) ([]User, error) {
	var users = make([]User, 0)

	rows, err := r.db.QueryContext(ctx, "SELECT * FROM User WHERE deleted_at IS NULL;")
	if err != nil {
		return []User{}, err
	}
	defer rows.Close()

	for rows.Next() {
		var user User
		var deletedAt sql.NullString

		if err = rows.Scan(
			&user.ID,
			&user.Name,
			&user.Email,
			&user.Password,
			&user.CreatedAt,
			&deletedAt,
		); err != nil {
			slog.Error(err.Error())
			return nil, err
		}

		users = append(users, user)
	}

	if err = rows.Err(); err != nil {
		slog.Error(err.Error())
		return []User{}, err
	}

	return users, nil
}

func (r Repository) Get(ctx context.Context, id int) (User, error) {
	var user User
	var deletedAt sql.NullString

	row := r.db.QueryRowContext(ctx, "SELECT * FROM User WHERE id = ? AND deleted_at IS NULL;", id)

	if err := row.Scan(
		&user.ID,
		&user.Name,
		&user.Email,
		&user.Password,
		&user.CreatedAt,
		&deletedAt,
	); err != nil {
		slog.Error(err.Error())

		if errors.Is(err, sql.ErrNoRows) {
			return User{}, err
		}

		return User{}, err
	}

	return user, nil
}

func (r Repository) GetByEmail(ctx context.Context, email string) (User, error) {
	var user User

	row := r.db.QueryRowContext(ctx, "SELECT password FROM User WHERE email = ? AND deleted_at IS NULL;", email)

	if err := row.Scan(
		&user.Password,
	); err != nil {
		slog.Error(err.Error())

		if errors.Is(err, sql.ErrNoRows) {
			return User{}, err
		}

		return User{}, err
	}

	return user, nil
}

func (r Repository) Delete(ctx context.Context, id int) error {
	_, err := r.db.ExecContext(ctx, "UPDATE User SET deleted_at = ? WHERE id = ?", time.Now(), id)
	if err != nil {
		slog.Error(err.Error())
		return err
	}

	return nil
}
