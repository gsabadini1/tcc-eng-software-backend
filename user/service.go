package user

import "context"

type Service struct {
	repository Storage
}

func NewService(r Storage) Service {
	return Service{
		repository: r,
	}
}

func (s Service) Create(ctx context.Context, input User) (User, error) {
	item, err := s.repository.Create(ctx, input)
	if err != nil {
		return User{}, err
	}

	return item, nil
}

func (s Service) Login(ctx context.Context, input User) (bool, error) {
	item, err := s.repository.GetByEmail(ctx, input.Email)
	if err != nil {
		return false, err
	}

	if input.Password == item.Password {
		return true, nil
	}

	return false, nil
}

func (s Service) List(ctx context.Context) ([]User, error) {
	list, err := s.repository.List(ctx)
	if err != nil {
		return []User{}, err
	}

	return list, nil
}

func (s Service) Get(ctx context.Context, id int) (User, error) {
	item, err := s.repository.Get(ctx, id)
	if err != nil {
		return User{}, err
	}

	return item, nil
}

func (s Service) Update(ctx context.Context, id int, input User) error {
	err := s.repository.Update(ctx, id, input)
	if err != nil {
		return err
	}

	return nil
}

func (s Service) Delete(ctx context.Context, id int) error {
	err := s.repository.Delete(ctx, id)
	if err != nil {
		return err
	}

	return nil
}
