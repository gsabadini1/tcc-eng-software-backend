package response

import (
	"encoding/json"
	"net/http"
)

type Response struct {
	result     any
	statusCode int
}

func NewResponse(result any, status int) Response {
	return Response{
		result:     result,
		statusCode: status,
	}
}

func (r Response) Send(w http.ResponseWriter) error {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(r.statusCode)

	return json.NewEncoder(w).Encode(r.result)
}
