up:
	@docker-compose up -d

down:
	@docker-compose down --remove-orphans --volumes

logs:
	@docker-compose logs -f

run:
	go run main.go