CREATE TABLE User
(
    id         integer auto_increment primary key unique,
    name       varchar(50)  default null,
    email      varchar(50)  default null,
    password   varchar(100) default null,
    created_at timestamp,
    deleted_at timestamp
);

CREATE TABLE LightingPoint
(
    id         integer auto_increment primary key unique,
    height     float        default null,
    lamp_type  varchar(100) default null,
    lamp_power integer      default null,
    latitude   varchar(100) default null,
    longitude  varchar(100) default null,
    created_at timestamp,
    deleted_at timestamp
);

CREATE TABLE ServiceOrder
(
    id                integer auto_increment primary key unique,
    lighting_point_id integer not null,
    technical_manager varchar(100) default null,
    description       text        default null,
    status            varchar(20) default null,
    start_date        varchar(10) default null,
    end_date          varchar(20) default null,
    created_at        timestamp,
    deleted_at        timestamp,

    FOREIGN KEY (lighting_point_id) REFERENCES LightingPoint (id)
);