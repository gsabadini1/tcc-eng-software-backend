package main

import (
	"github.com/rs/cors"
	"log"
	"net/http"
	"tcc-software-engineer/infra/database"
	"tcc-software-engineer/report"
	"tcc-software-engineer/serviceorder"
	"tcc-software-engineer/user"

	"tcc-software-engineer/lightingpoint"
)

func main() {
	var (
		mux = http.NewServeMux()
		db  = database.Open()
	)

	handler := cors.AllowAll().Handler(mux)

	var (
		lightingPointController = lightingpoint.NewController(lightingpoint.NewService(lightingpoint.NewRepository(db)))
		serviceOrderController  = serviceorder.NewController(serviceorder.NewService(serviceorder.NewRepository(db)))
		userController          = user.NewController(user.NewService(user.NewRepository(db)))
		reportController        = report.NewController(report.NewService(report.NewRepository(db)))
	)

	mux.HandleFunc("POST /v1/lighting-point", lightingPointController.Create)
	mux.HandleFunc("GET /v1/lighting-point/{id}", lightingPointController.Get)
	mux.HandleFunc("GET /v1/lighting-point", lightingPointController.List)
	mux.HandleFunc("PATCH /v1/lighting-point/{id}", lightingPointController.Update)
	mux.HandleFunc("DELETE /v1/lighting-point/{id}", lightingPointController.Delete)

	mux.HandleFunc("POST /v1/service-order", serviceOrderController.Create)
	mux.HandleFunc("GET /v1/service-order/{id}", serviceOrderController.Get)
	mux.HandleFunc("GET /v1/service-order", serviceOrderController.List)
	mux.HandleFunc("PATCH /v1/service-order/{id}", serviceOrderController.Update)
	mux.HandleFunc("DELETE /v1/service-order/{id}", serviceOrderController.Delete)

	mux.HandleFunc("POST /v1/user", userController.Create)
	mux.HandleFunc("GET /v1/user/{id}", userController.Get)
	mux.HandleFunc("GET /v1/user", userController.List)
	mux.HandleFunc("PATCH /v1/user/{id}", userController.Update)
	mux.HandleFunc("DELETE /v1/user/{id}", userController.Delete)
	mux.HandleFunc("POST /v1/user/login", userController.Login)

	mux.HandleFunc("GET /v1/report/by-status", reportController.GetByStatus)

	log.Fatal(http.ListenAndServe(":8080", handler))
}
