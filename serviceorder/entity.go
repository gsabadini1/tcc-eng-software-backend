package serviceorder

import (
	"time"
)

type Status string

const (
	Open  Status = "ABERTO"
	Close Status = "FECHADO"
)

type ServiceOrder struct {
	ID               int       `json:"id"`
	LightingPointID  int       `json:"lighting_point_id"`
	Status           Status    `json:"status"`
	TechnicalManager string    `json:"technical_manager"`
	Description      string    `json:"description"`
	StartDate        string    `json:"start_date"`
	EndDate          string    `json:"end_date"`
	CreatedAt        time.Time `json:"-"`
	DeletedAt        time.Time `json:"-"`
}
