package serviceorder

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log/slog"
	"strings"
	"time"
)

type Storage interface {
	Create(context.Context, ServiceOrder) (ServiceOrder, error)
	Update(context.Context, int, ServiceOrder) error
	List(context.Context) ([]ServiceOrder, error)
	Get(context.Context, int) (ServiceOrder, error)
	Delete(context.Context, int) error
}

type Repository struct {
	db *sql.DB
}

func NewRepository(db *sql.DB) Repository {
	return Repository{
		db: db,
	}
}

func (r Repository) Create(ctx context.Context, input ServiceOrder) (ServiceOrder, error) {
	exec, err := r.db.ExecContext(ctx, "INSERT INTO ServiceOrder (lighting_point_id, technical_manager, description, status, start_date, end_date, created_at) VALUES (?, ?, ?, ?, ?, ?, ?);",
		input.LightingPointID,
		input.TechnicalManager,
		input.Description,
		input.Status,
		input.StartDate,
		input.EndDate,
		time.Now(),
	)
	if err != nil {
		slog.Error(err.Error())
		return ServiceOrder{}, err
	}

	id, err := exec.LastInsertId()
	if err != nil {
		slog.Error(err.Error())
		return ServiceOrder{}, err
	}

	input.ID = int(id)

	return input, nil
}

func (r Repository) Update(ctx context.Context, id int, input ServiceOrder) error {
	var args []string

	if input.LightingPointID != 0 {
		args = append(args, fmt.Sprintf("lighting_point_id = %d", input.LightingPointID))
	}

	if input.TechnicalManager != "" {
		args = append(args, fmt.Sprintf("technical_manager = '%s'", input.TechnicalManager))
	}

	if input.Description != "" {
		args = append(args, fmt.Sprintf("description = '%s'", input.Description))
	}

	if input.Status != "" {
		args = append(args, fmt.Sprintf("status = '%s'", input.Status))
	}

	if input.StartDate != "" {
		args = append(args, fmt.Sprintf("start_date = '%s'", input.StartDate))
	}

	if input.EndDate != "" {
		args = append(args, fmt.Sprintf("end_date = '%s'", input.EndDate))
	}

	var query = fmt.Sprintf("UPDATE ServiceOrder SET %s WHERE id = ?;", strings.Join(args, ", "))

	_, err := r.db.ExecContext(ctx, query, id)
	if err != nil {
		slog.Error(err.Error())
		return err
	}

	return nil
}

func (r Repository) List(ctx context.Context) ([]ServiceOrder, error) {
	var serviceOrders = make([]ServiceOrder, 0)

	rows, err := r.db.QueryContext(ctx, "SELECT * FROM ServiceOrder WHERE deleted_at IS NULL;")
	if err != nil {
		return []ServiceOrder{}, err
	}
	defer rows.Close()

	for rows.Next() {
		var serviceOrder ServiceOrder
		var deletedAt sql.NullString

		if err = rows.Scan(
			&serviceOrder.ID,
			&serviceOrder.LightingPointID,
			&serviceOrder.TechnicalManager,
			&serviceOrder.Description,
			&serviceOrder.Status,
			&serviceOrder.StartDate,
			&serviceOrder.EndDate,
			&serviceOrder.CreatedAt,
			&deletedAt,
		); err != nil {
			slog.Error(err.Error())
			return nil, err
		}

		serviceOrders = append(serviceOrders, serviceOrder)
	}

	if err = rows.Err(); err != nil {
		slog.Error(err.Error())
		return nil, err
	}

	return serviceOrders, nil
}

func (r Repository) Get(ctx context.Context, id int) (ServiceOrder, error) {
	var serviceOrder ServiceOrder
	var deletedAt sql.NullString

	row := r.db.QueryRowContext(ctx, "SELECT * FROM ServiceOrder WHERE id = ? AND deleted_at IS NULL;", id)

	if err := row.Scan(
		&serviceOrder.ID,
		&serviceOrder.LightingPointID,
		&serviceOrder.TechnicalManager,
		&serviceOrder.Description,
		&serviceOrder.Status,
		&serviceOrder.StartDate,
		&serviceOrder.EndDate,
		&serviceOrder.CreatedAt,
		&deletedAt,
	); err != nil {
		slog.Error(err.Error())

		if errors.Is(err, sql.ErrNoRows) {
			return ServiceOrder{}, err
		}

		return ServiceOrder{}, err
	}

	return serviceOrder, nil
}

func (r Repository) Delete(ctx context.Context, id int) error {
	_, err := r.db.ExecContext(ctx, "UPDATE ServiceOrder SET deleted_at = ? WHERE id = ?", time.Now(), id)
	if err != nil {
		slog.Error(err.Error())
		return err
	}

	return nil
}
