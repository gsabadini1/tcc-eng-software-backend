package serviceorder

import (
	"encoding/json"
	"log/slog"
	"net/http"
	"strconv"
	"tcc-software-engineer/response"
)

type Controller struct {
	service Service
}

func NewController(s Service) Controller {
	return Controller{
		service: s,
	}
}

func (c Controller) Create(w http.ResponseWriter, r *http.Request) {
	var input ServiceOrder
	if err := json.NewDecoder(r.Body).Decode(&input); err != nil {
		slog.Error(err.Error())
		response.NewResponse(
			response.NewError("error decoding payload"),
			http.StatusInternalServerError,
		).Send(w)
		return
	}
	defer r.Body.Close()

	output, err := c.service.Create(r.Context(), input)
	if err != nil {
		response.NewResponse(
			response.NewError("error creating service order"),
			http.StatusInternalServerError,
		).Send(w)
		return
	}

	response.NewResponse(output, http.StatusCreated).Send(w)
	return
}

func (c Controller) Update(w http.ResponseWriter, r *http.Request) {
	var id = r.PathValue("id")

	if id == "" {
		response.NewResponse(
			response.NewError("invalid id"),
			http.StatusBadRequest,
		).Send(w)

		return
	}

	idInt, err := strconv.Atoi(id)
	if err != nil {
		response.NewResponse(
			response.NewError("error converting id"),
			http.StatusBadRequest,
		).Send(w)

		return
	}

	var input ServiceOrder
	if err := json.NewDecoder(r.Body).Decode(&input); err != nil {
		response.NewResponse(
			response.NewError("error decoding payload"),
			http.StatusInternalServerError,
		).Send(w)
		return
	}

	err = c.service.Update(r.Context(), idInt, input)
	if err != nil {
		response.NewResponse(
			response.NewError("error updating service order"),
			http.StatusInternalServerError,
		).Send(w)
		return
	}

	response.NewResponse(nil, http.StatusNoContent).Send(w)
	return
}

func (c Controller) Get(w http.ResponseWriter, r *http.Request) {
	var id = r.PathValue("id")

	if id == "" {
		response.NewResponse(
			response.NewError("invalid id"),
			http.StatusBadRequest,
		).Send(w)

		return
	}

	idInt, err := strconv.Atoi(id)
	if err != nil {
		response.NewResponse(
			response.NewError("error converting id"),
			http.StatusBadRequest,
		).Send(w)

		return
	}

	output, err := c.service.Get(r.Context(), idInt)
	if err != nil {
		response.NewResponse(
			response.NewError("error getting service order"),
			http.StatusInternalServerError,
		).Send(w)
		return
	}

	response.NewResponse(output, http.StatusOK).Send(w)
	return
}

func (c Controller) List(w http.ResponseWriter, r *http.Request) {
	output, err := c.service.List(r.Context())
	if err != nil {
		response.NewResponse(
			response.NewError("error listing service order"),
			http.StatusInternalServerError,
		).Send(w)
		return
	}

	response.NewResponse(output, http.StatusOK).Send(w)
	return
}

func (c Controller) Delete(w http.ResponseWriter, r *http.Request) {
	var id = r.PathValue("id")

	if id == "" {
		response.NewResponse(
			response.NewError("invalid id"),
			http.StatusBadRequest,
		).Send(w)

		return
	}

	idInt, err := strconv.Atoi(id)
	if err != nil {
		response.NewResponse(
			response.NewError("error converting id"),
			http.StatusBadRequest,
		).Send(w)

		return
	}

	err = c.service.Delete(r.Context(), idInt)
	if err != nil {
		response.NewResponse(
			response.NewError("error deleting service order"),
			http.StatusInternalServerError,
		).Send(w)
		return
	}

	response.NewResponse(nil, http.StatusNoContent).Send(w)
	return
}
